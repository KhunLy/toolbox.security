﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace ToolBoxes.Security.JWT
{
    public class TokenService
    {
        private readonly string _signature;
        private readonly string _issuer;
        private readonly string _audience;
        private readonly int _delay;
        // service qui ecrit ou lit les tokens
        private readonly JwtSecurityTokenHandler _handler;

        public TokenService()
        {
            _signature = ConfigurationManager.AppSettings.Get("signature");
            _issuer = ConfigurationManager.AppSettings.Get("issuer");
            _audience = ConfigurationManager.AppSettings.Get("audience");
            int.TryParse(ConfigurationManager.AppSettings.Get("delay"), out _delay);
            if (_signature == null || _delay <= 0)
                throw new ConfigurationErrorsException();
            _handler = new JwtSecurityTokenHandler();
        }

        public TokenService(string signature, string issuer, string audience, int delay)
        {
            _signature = signature;
            _issuer = issuer;
            _audience = audience;
            _delay = delay;
            if (_signature == null || _delay <= 0)
                throw new ConfigurationErrorsException();
            _handler = new JwtSecurityTokenHandler();
        }

        public string EncodeToken<T>(T o)
        {
            // crée le token
            JwtSecurityToken token = new JwtSecurityToken(
                // remplir le token
                _issuer,
                _audience,
                GetClaims(o),
                DateTime.Now,
                DateTime.Now.AddSeconds(_delay),
                GetSigningCredentials()
            );

            // retourne le token en string
            return _handler.WriteToken(token);
        }

        private SigningCredentials GetSigningCredentials()
        {
            return new SigningCredentials(
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_signature)),
                SecurityAlgorithms.HmacSha256
            );
        }

        private IEnumerable<Claim> GetClaims<T>(T o)
        {
            IEnumerable<PropertyInfo> props = typeof(T).GetProperties();
            return props.Select(p => new Claim(p.Name, p.GetValue(o).ToString()));
        }

        public ClaimsPrincipal DecodeToken(string token)
        {
            try
            {
                return _handler.ValidateToken(token, GetParameters(), out SecurityToken key);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private TokenValidationParameters GetParameters()
        {
            return new TokenValidationParameters {
                ValidateIssuer = _issuer != null,
                ValidIssuer = _issuer,
                ValidateAudience = _audience != null,
                ValidAudience = _audience,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_signature))
            };
        }
    }
}
