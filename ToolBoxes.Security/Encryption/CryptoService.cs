﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ToolBoxes.Security.Encryption
{
    public class CryptoService
    {
        private RSACryptoServiceProvider rsa;


        public string GetPublicKey()
        {
            return rsa.ToXmlString(false);
        }


        public CryptoService(int size = 2048)
        {
            // crée la clé publique et la clé privée
            rsa = new RSACryptoServiceProvider(size);
        }

        public CryptoService(string pubKey)
        {
            rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(pubKey);
        }

        public byte[] Crypt(string raw)
        {
            return rsa.Encrypt(Encoding.UTF8.GetBytes(raw), true);
        }

        public string Uncrypt(byte[] crypted)
        {
            try
            {
                return Encoding.UTF8.GetString(rsa.Decrypt(crypted, true));
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
