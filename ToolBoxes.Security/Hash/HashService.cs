﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ToolBoxes.Security.Hash
{
    public class HashService
    {
        private readonly HashAlgorithm algo;

        public HashService(HashAlgorithm algo)
        {
            this.algo = algo;
        }

        public byte[] Encode(string toHash)
        {
            return algo.ComputeHash(Encoding.UTF8.GetBytes(toHash));
        }
    }
}
