﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToolBoxes.Security.JWT;
using WebAPI.Test.Models;

namespace WebAPI.Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private static List<UserDTO> _users = new List<UserDTO>
        {
            new UserDTO { Id = 1, Username = "khun", Password = "ly", Role = "ADMIN" },
            new UserDTO { Id = 2, Username = "mike", Password = "person", Role = "ADMIN" },
            new UserDTO { Id = 3, Username = "thierry", Password = "morre", Role = "CUSTOMER" },
            new UserDTO { Id = 4, Username = "steve", Password = "lorent", Role = "SIMPLE_USER" },
        };

        [HttpPost("Login")]
        public IActionResult Login(LoginDTO model, [FromServices]TokenService ts)
        {
            UserDTO connectedUser = _users
                .FirstOrDefault(u => u.Username == model.Username && u.Password == model.Password);
            if (connectedUser == null)
                return BadRequest();
            else
            {
                connectedUser.Password = string.Empty;
                string token = ts.EncodeToken(connectedUser);
                return Ok(token);
            }
        }
    }
}