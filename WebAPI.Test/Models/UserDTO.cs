﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Test.Models
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
    }
}
