﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ToolBoxes.Security.JWT;

namespace WebAPI.Test.Filters
{
    public class ApiAuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private string[] _roles;
        public ApiAuthorizeAttribute(params string[] roles)
        {
            _roles = roles;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            IHeaderDictionary headers = context.HttpContext.Request.Headers;
            var authorizations = headers.FirstOrDefault(x => x.Key == "Authorization");
            IEnumerable<string> values = authorizations.Value;
            string token = values?.FirstOrDefault(v => v.StartsWith("Bearer "))?.Replace("Bearer ", "");
            if (token == null) 
                context.Result = new UnauthorizedResult();
            // get TokenService
            TokenService ts = context.HttpContext.RequestServices
                .GetService(typeof(TokenService)) as TokenService;
            ClaimsPrincipal principal = ts.DecodeToken(token);
            if (principal == null || !principal.Identity.IsAuthenticated)
                context.Result = new UnauthorizedResult();
            else
            {
                // verifier les roles
                bool authorized = false;
                foreach (string role in _roles)
                {
                    if (principal.IsInRole(role))
                        authorized = true;
                }
                if (!authorized)
                    context.Result = new UnauthorizedResult();
                else
                {
                    context.HttpContext.User = principal;
                }
                //valider le token à l'aide de notre token service
                //if(valid) ne fait rien
                // else // bloquer
            }

        }
    }
}
