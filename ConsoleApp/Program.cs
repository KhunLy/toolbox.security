﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ToolBoxes.Security.Encryption;
using ToolBoxes.Security.Hash;
using ToolBoxes.Security.JWT;

namespace ConsoleApp
{
    class User 
    {
        public int Id { get; set; }
        public string Role { get; set; }
        public string Username { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //TokenService s = new TokenService();
            //User u = new User { Id = 1, Role = "CUSTOMER", Username = "LyKhun" };
            //string token = s.EncodeToken(u);
            //Console.WriteLine(token);
            //if (s.DecodeToken(token) != null)
            //{
            //    Console.WriteLine("OK");
            //}
            //else
            //{
            //    Console.WriteLine("KO");
            //}

            //CryptoService s = new CryptoService();

            //string pubKey = s.GetPublicKey();

            //CryptoService s2 = new CryptoService(pubKey);

            //string original = "une phrase au hazard";

            //Console.WriteLine("original : " + original);

            //byte[] crypted =  s2.Crypt(original);

            //Console.WriteLine("Crypted : " + Encoding.UTF8.GetString(crypted));

            //string decrypted = s.Uncrypt(crypted);

            //Console.WriteLine("Derypted : " + decrypted);


            HashService s = new HashService(new SHA256CryptoServiceProvider());
            string guid1 = Guid.NewGuid().ToString();
            string guid2 = Guid.NewGuid().ToString();
            byte[] encoded = s.Encode("test1234=" + Guid.NewGuid().ToString());
            byte[] encoded2 = s.Encode("test1234=" + Guid.NewGuid().ToString());
            Console.WriteLine(Encoding.UTF8.GetString(encoded));
            Console.WriteLine(Encoding.UTF8.GetString(encoded2));

            Console.ReadKey();
        }
    }
}
